

INSERT INTO roles (id,name) VALUES (1, 'ROLE_CONTROLLER');
INSERT INTO roles (id,name) VALUES (2, 'ROLE_CONTRIBUTOR');
INSERT INTO roles (id,name) VALUES (3, 'ROLE_MANAGER');

INSERT INTO users (token,username,password,enabled,account_non_expired, account_non_locked, credentials_non_expired) VALUES ('eforstner','eforstner', '$2a$10$AIUufK8g6EFhBcumRRV2L.AQNz3Bjp7oDQVFiO5JJMBFZQ6x2/R/2', 1, 1, 1, 1);
INSERT INTO users (token,username,password,enabled,account_non_expired, account_non_locked, credentials_non_expired) VALUES ('sparzer','sparzer', '$2a$10$AIUufK8g6EFhBcumRRV2L.AQNz3Bjp7oDQVFiO5JJMBFZQ6x2/R/2', 1, 1, 1, 1);
INSERT INTO users (token,username,password,enabled,account_non_expired, account_non_locked, credentials_non_expired) VALUES ('fwurm','fwurm', '$2a$10$AIUufK8g6EFhBcumRRV2L.AQNz3Bjp7oDQVFiO5JJMBFZQ6x2/R/2', 1, 1, 1, 1);
INSERT INTO users (token,username,password,enabled,account_non_expired, account_non_locked, credentials_non_expired) VALUES ('djabornig','djabornig', '$2a$10$AIUufK8g6EFhBcumRRV2L.AQNz3Bjp7oDQVFiO5JJMBFZQ6x2/R/2', 1, 1, 1, 1);

INSERT INTO user_roles (token,username) VALUES (1, 'eforstner');
INSERT INTO user_roles (token,username) VALUES (2, 'sparzer');
INSERT INTO user_roles (token,username) VALUES (3, 'fwurm');