package at.faw.schachinger.bpf.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Entity
@Table(name = "users")
public class User implements UserDetails {

    private String token;
    private String username;
    private String password;
    private boolean enabled;

    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;

    private List<Role> roles = new ArrayList<>();

    public User() {
    }

    @JsonCreator
    public User(@JsonProperty("token") String token,
         @JsonProperty("username") String username,
         @JsonProperty("password") String password,
         @JsonProperty("enabled") boolean enabled) {
        super();
        this.token = requireNonNull(token);
        this.username = requireNonNull(username);
        this.password = requireNonNull(password);
        this.enabled = enabled;
    }

    @Id
    @Column(name = "username", unique = true,
            nullable = false)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password",
            nullable = false)
    @JsonIgnore
    @Override
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "enabled", nullable = false)
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "token", unique = true,
            nullable = false)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @JsonIgnore
    @Override
    @Transient
    public Collection<GrantedAuthority> getAuthorities() {
        //return new ArrayList<>();
        String roles = new String();
        for (Role role : this.roles) {
            roles = roles.concat(role.getName()+",");
        }
        List<GrantedAuthority> grantedAuths =
                AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
        return grantedAuths;
    }

    @JsonIgnore
    @Override
    @Column(nullable = true)
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @JsonIgnore
    @Override
    @Column(nullable = true)
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @JsonIgnore
    @Override
    @Column(nullable = true)
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name="username"),
            inverseJoinColumns = @JoinColumn(name="token")
    )
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    //Getters and setters ommitted for brevity

    public void addRole(Role role) {
        roles.add(role);
        role.getUsers().add(this);
    }

    public void removeRole(Role role) {
        roles.remove(role);
        role.getUsers().remove(this);
    }
}
