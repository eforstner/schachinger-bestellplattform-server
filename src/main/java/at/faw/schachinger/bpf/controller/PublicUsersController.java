package at.faw.schachinger.bpf.controller;

import at.faw.schachinger.bpf.model.User;
import at.faw.schachinger.bpf.repository.UserRepository;
import at.faw.schachinger.bpf.service.authentication.UserAuthenticationService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@RestController
    @RequestMapping("/public/users")
    @FieldDefaults(level = PRIVATE, makeFinal = true)
    @AllArgsConstructor(access = PACKAGE)
    public final class PublicUsersController {
        @NonNull
        UserAuthenticationService authentication;
        @NonNull
        UserRepository users;

    @PostMapping("/register")
    String register(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password) {
        users.save(new User (username,username,password, true));

        return login(username, password);
    }

        @PostMapping("/login")
        String login(
                @RequestParam("username") final String username,
                @RequestParam("password") final String password) {
            return authentication
                    .login(username, password)
                    .orElseThrow(() -> new RuntimeException("invalid login and/or password"));
        }
    }
