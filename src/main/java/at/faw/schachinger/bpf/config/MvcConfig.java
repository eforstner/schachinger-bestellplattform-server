package at.faw.schachinger.bpf.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@ComponentScan
public class MvcConfig implements WebMvcConfigurer {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {

//		registry.addRedirectViewController("/api/v2/api-docs", "/v2/api-docs");
//		registry.addRedirectViewController("/api/swagger-resources/configuration/ui",
//				"/swagger-resources/configuration/ui");
//		registry.addRedirectViewController("/api/swagger-resources/configuration/security",
//				"/swagger-resources/configuration/security");
//		registry.addRedirectViewController("/api/swagger-resources", "/swagger-resources");

		registry.addViewController("/home").setViewName("home");
		registry.addViewController("/").setViewName("home");
		registry.addViewController("/hello").setViewName("hello");
		registry.addViewController("/login").setViewName("login");
		registry.addViewController("/error").setViewName("error");
	}

}
