package at.faw.schachinger.bpf.repository;


import at.faw.schachinger.bpf.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.annotation.Secured;

import java.util.Optional;

@Secured("ROLE_CONTROLLER")
@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserRepository extends CrudRepository<User, String> {

    User findByUsername(@Param("username") String username);
    User save(User user);
    Optional<User> findByToken(String token);

}
