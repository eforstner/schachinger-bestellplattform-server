package at.faw.schachinger.bpf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchachingerBestellplattformServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchachingerBestellplattformServerApplication.class, args);
	}
}
