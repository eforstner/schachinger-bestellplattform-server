package at.faw.schachinger.bpf;

import at.faw.schachinger.bpf.model.Role;
import at.faw.schachinger.bpf.model.User;
import at.faw.schachinger.bpf.repository.RoleRepository;
import at.faw.schachinger.bpf.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Before
    public void setUp() throws Exception {
        Role adminrole = new Role(1, "ROLE_ADMIN");

        this.roleRepository.save(adminrole);

        Set<Role> roles = new HashSet<Role>(0);
        roles.add(adminrole);
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        User user1= new User("Alice", "Alice", encoder.encode("test"), true);
        User user2= new User("Bob", "Bob", encoder.encode("test"), true);
        //save user, verify has ID value after save
        this.userRepository.save(user1);
        this.userRepository.save(user2);
    }
    @Test
    public void testFetchData(){
        /*Test data retrieval*/
        User userA = userRepository.findByUsername("Bob");
        assertNotNull(userA);
        assertEquals("Bob", userA.getUsername());
        /*Get all products, list should only have two*/
        Iterable<User> users = userRepository.findAll();
        int count = 0;
        for(User p : users){
            count++;
        }
        assertEquals(count, 2);
    }
}